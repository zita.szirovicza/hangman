const stdin = process.stdin;
stdin.setRawMode(true);
stdin.setEncoding('utf8');
const readline = require('readline-sync');

const dictionary = ['flow', 'computer', 'javascript', 'spider', 'dragon', 'french', 'road', 'cats', 'bugs'];
let word = '';
const guessedword = [];
const generateword = (arr) => {
  const i = Math.floor(Math.random() * 9);
  word = arr[i];
};
const printgame = () => {
  generateword(dictionary);
  const x = word.length;
  for (let i = 0; i < x; i++) {
    guessedword.push('_');
  }
  console.log(guessedword.join(' '));
};
const makeguess = () => {
  let life = word.length / 2;
  console.log('Life:', life);
  console.log('(your life is half the length of the word)');
  while (unsolved() === true) {
    const l = readline.keyIn();
    if (word.indexOf(l) >= 0) {
      const x = word.indexOf(l);
      guessedword[x] = l;
      console.log(guessedword.join(' '));
    }
    if (word.indexOf(l) === -1) {
      life = life - 1;
      if (life === 0) {
        console.log('YOU RAN OUT OF LIVES, GAME OVER');
        //again();
        process.exit();
      }
      console.log('invalid character, try again', 'life:', life);
    }
  }
};
const unsolved = () => {
  if (guessedword.indexOf('_') >= 0) {
    return true;
  } else {
    return false;
  }
};
const game = () => {
  console.clear();
  printgame();
  makeguess();
};
const again = () => {
  const startover = readline.keyInYN('START OVER?');
  if (startover) {
    game();
  } else {
    process.exit();
  }
}
game();
